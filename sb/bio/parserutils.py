__author__ = "Biju Joseph"

import csv
import logging

logger = logging.getLogger('bio')


class BioDataHandler:
    """
    A unified interface for callbacks/events from parser.
    """

    def __init__(self, datafile):
        """
         Initialize the handler
        :param datafile: the file to be processed
        """
        logging.debug("Input File: %s", datafile)
        self.datafile = datafile

    def on_begin(self, aparser):
        """Will be invoked at the beginning of the file
            :param aparser: the parser in context
        """
        logging.info("parsing: %s", aparser)

    def on_end(self, aparser):
        """Will be invoked at the end of the file
            :param aparser: the parser in context
        """
        logging.info("finished: %s", aparser)

    def on_data(self, line, data):
        """Will be invoked for every data item in the data file
            :param line: row number of a given CSV file
            :param data: each row data of a given CSV file
        """
        logging.info("data-line (%s): %s", line, data)

    @staticmethod
    def retrieve_accession(syn):
        """
        Will retrieve accession from the syntax
        :param syn: a syntax with accession
        :return: accession number
        """
        parts = syn.split(":")
        if len(parts) > 1:
            return parts[0]
        return

    @staticmethod
    def remove_accession(syn):
        """
        Will remove accession from the syntax
        :param syn: a syntax with accession
        :return: part after accession
        """
        parts = syn.split(":")
        if len(parts) > 1:
            return parts[1]
        return


class BioParser:
    """
        A unified interface for parsers
    """

    def __init__(self, handler):
        self.handler = handler

    def parse(self):
        """
         Sub classes should provide appropriate logic for parsing datafile
        """
        logging.debug("Parsing %s", self.datafile)


class BioCSVParser(BioParser):
    """
    A parser implementation that can read CSV/TSV data files and will
    invoke the callback functions defined by the handler for further processing
    """

    def __init__(self, handler):
        """
        Initialize the csv parser
        :param handler: the handler to use
        """
        super(BioCSVParser, self).__init__(handler)

    def parse(self):
        """ Contains the logic to read CSV file """
        self.handler.on_begin(self)
        with open(self.handler.datafile) as f:
            dialect = csv.Sniffer().sniff(f.read(100))
            f.seek(0)
            reader = csv.DictReader(f, dialect=dialect)

            try:
                for row in reader:
                    self.handler.on_data(reader.line_num, row)
            except csv.Error as e:
                logging.error("Error processing file: %s line %d: %s",
                              self.handler.csvfile, reader.line_num, e)

        # Reached end of CSV data file
        self.handler.on_end(self)


class BioXMLSnifferParser(BioParser):
    """
    A parser implementation that can read large XML files, sniff for the tags of
    interest and will then invoke callback functions with the xml snippet
    for further processing.
    Note: will only works with well formatted XMLs where the tags (at least
    the ones of interest) are in separate lines.
    """

    def __init__(self, tag_of_interest, handler):
        """ Initializes the parser
        :param tag_of_interest: the tag of interest
        :param handler: the callback handler
        """
        self.tag_of_interest = tag_of_interest
        super(BioXMLSnifferParser, self).__init__(handler)

    def parse(self):
        """ Will use plain text streaming to read snippets of interest
        """
        self.handler.on_begin(self)
        n = 0
        tag_start_line = 0
        xml = ''
        with open(self.handler.xmlfile, 'r') as fhandle:
            concat = False
            start_tag1 = '<{0} '.format(self.tag_of_interest)
            start_tag2 = '<{0}>'.format(self.tag_of_interest)
            end_tag = '</{0}>'.format(self.tag_of_interest)
            for line in fhandle:
                n += 1
                if start_tag1 in line or start_tag2 in line:
                    xml = line
                    concat = True
                    tag_start_line = n
                elif end_tag in line:
                    xml += line
                    concat = False
                    self.handler.on_data(tag_start_line, xml)
                    xml = None
                elif concat:
                    xml += line

        # Reached end of file
        self.handler.on_end(self)
