__author__ = "Biju Joseph"
import logging

logger = logging.getLogger('repo')


class Repository:
    """
     Provides a unified interface for repositories
    """
    def __init__(self, name):
        """
        Will initialize the repository
        :param name: name of the data store
        """
        self.name = name

    def persist(self, obj):
        """Persists the given JSON in mongo collection
          :param obj: the object to persist
        """
        logging.debug("Persisting : %s", obj)

    def load(self, selector, sort=[]):
        """
        Retrieves the objects data store
        :param selector: filter criteria
        :param sort: sorting order
        """
        logging.debug("Loading object by selector: %s", selector)


class BatchRepository(Repository):
    """
        A repository that decorates another one with batching support.
        This is usually useful in bulk loading of large data set
    """

    def __init__(self, batch_size, repo):
        """
        Will initialize the repository
        :param batch_size: The batch size needed
        :param repo: the repository to decorate
        """
        self.repo = repo
        self.batch = []
        self.batch_size = batch_size if batch_size > 1 else 1
        super().__init__(repo.name)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.flush()

    def flush(self):
        """
        Flushes the internal cache, by calling persist on actual repository
        Note : We can explicitly call flush when no more object are to be added
        """
        result = self.repo.persist(self.batch)
        del self.batch[:]
        return result

    def persist(self, obj):
        """ Will keep adding objects to internal cache and will persist once the cache is full
        :param obj: object to be persisted
        :return: None, if added to batch, else will return the output of flush
        """
        self.batch.append(obj)
        if len(self.batch) >= self.batch_size:
            logging.debug("Batch full, going to flush : %d", len(self.batch))
            return self.flush()

    def load(self, selector, sort=[]):
        """
        Will delegate to the repository that is decorated
        """
        return self.repo.load(selector, sort)
