__author__ = "Biju Joseph"
import logging

import pymongo

from sb.repository.repositories import Repository

logger = logging.getLogger('mongorepo')


class MongoDbRepository(Repository):
    """ A facade that provides functionality to manage objects in MongoDB collection
    Attributes:
        db: the database to use
        collection_name: the collection that is repository is managing
    """

    def __init__(self, db, collection_name):
        """ Initializes the repo  """
        self.db = db
        super(MongoDbRepository, self).__init__(collection_name)

    def persist(self, obj):
        """
        Persists the given JSON in MongoDB collection
        :param obj: the object to persist
        :return: the identifier or identifiers
        """
        try:
            if isinstance(obj, (list, tuple)):
                result = self.db[self.name].insert_many(obj)
            else:
                result = self.db[self.name].insert_one(obj)
            return result
        except pymongo.errors.PyMongoError as e:
            logging.error("Error persisting data", e)
            raise e



    def load(self, selector, sort=[]):
        """
        Retrieves the objects from MongoDB collection
        :param selector: filter criteria
        :param sort: sorting order
        :return: all the objects that matched the filter criteria
        """
        try:
            results = []

            if not sort:
                cursor = self.db[self.name].find(selector)
            else:
                cursor = self.db[self.name].find(selector).sort(sort)
            for doc in cursor:
                results.append(doc)
            return results
        except pymongo.errors.PyMongoError as e:
            logging.error("Error loading data", e)
            raise e
