__author__ = "Biju Joseph"
import json
import logging
import os
import shutil
import xml.etree.ElementTree as ET
import zipfile
from xml.etree.ElementTree import ParseError

from bson import json_util
from dateutil import parser

from sb.bio.parserutils import BioDataHandler

logger = logging.getLogger('clinvar')


# Handles ClinVar XML data
class ClinVarDataHandler(BioDataHandler):
    """Provides functionality to process ClinVarSet snippets.
    Will extract data, transform to JSON and keep it in MongoDB database ingest.
    Attributes:
        datafile: a zip file containing ClinVar XML data export
        repository: a repository to use for persisting json records
    """

    def __init__(self, datafile, repo):
        super(ClinVarDataHandler, self).__init__(datafile)
        self.repository = repo

    def on_begin(self, aparser):
        """Must unzip the Zip file and add then populate the xmlfile"""
        with open(self.datafile, "rb") as zipsrc:
            zfile = zipfile.ZipFile(zipsrc)
            member = zfile.infolist()[0]
            xmlfile = member.filename
            with open(xmlfile, 'wb') as outfile, zfile.open(member) as infile:
                shutil.copyfileobj(infile, outfile)
            self.xmlfile = xmlfile

    def on_end(self, aparser):
        """ Will delete off temporary files """
        logging.info("deleting %s", self.xmlfile)
        try:
            os.remove(self.xmlfile)
        except OSError:
            pass

    def on_data(self, line, data):
        """
        Extract necessary data from ClinVarSet such as chromosome, gene, protein change etc.
        :param line: The line number representing the start of current ClinVarSet tag
        :param data: The ClinVarSet snippet
        """
        try:
            o = {}
            root = ET.fromstring(data)
            logging.debug("Processing Line : %d", line)
            rcv = root.find("./ReferenceClinVarAssertion/ClinVarAccession").attrib['Acc']
            variant = root.find(
                "./ReferenceClinVarAssertion/MeasureSet[@Type='Variant']/Measure[@Type='single nucleotide variant']")
            if variant == None:
                return

            classification_node = root.find(
                "./ReferenceClinVarAssertion/ClinicalSignificance/Description")
            classification = classification_node.text if classification_node != None else None

            review_date_node = root.find("./ReferenceClinVarAssertion/ClinicalSignificance").attrib['DateLastEvaluated']
            review_date = parser.parse(review_date_node) if review_date_node != None else None

            # handle gene
            gene_node = variant.find(
                "./MeasureRelationship[@Type='variant in gene']/Symbol/ElementValue[@Type='Preferred']")
            gene_symbol = gene_node.text if gene_node != None else None

            # handle chromosome
            chr_node = variant.find("./SequenceLocation[@Assembly='GRCh38']")
            chr = chr_node.attrib['Chr'] if chr_node != None else None
            start = chr_node.attrib['start'] if chr_node != None else None
            stop = chr_node.attrib['stop'] if chr_node != None else None
            ref = chr_node.attrib['referenceAllele'] if chr_node != None else '.'
            alt = chr_node.attrib['alternateAllele'] if chr_node != None else '.'

            # handle exon
            exon_node = variant.find(".//AttributeSet/Attribute[@Type='Location']")
            exon = BioDataHandler.remove_accession(exon_node.text) if exon_node != None else None

            # handle csyntaxes
            c_syntax_nodes = variant.findall(".//AttributeSet/Attribute[@Type='HGVS, coding, RefSeq']")
            c_syntaxes = []
            for c_syntax_node in c_syntax_nodes:
                c_syntaxes.append(c_syntax_node.text)

            # handle psyntaxes
            p_syntax_nodes = variant.findall(".//AttributeSet/Attribute[@Type='HGVS, protein, RefSeq']")
            p_syntaxes = []
            for p_syntax_node in p_syntax_nodes:
                p_syntaxes.append(p_syntax_node.text)

            o["clinVarRCV"] = rcv
            o["gene"] = gene_symbol
            o["classification"] = classification
            o["reviewedOn"] = review_date
            o['exon'] = exon
            o['chr'] = chr
            o['start'] = start
            o['stop'] = stop
            o['ref'] = ref
            o['alt'] = alt

            if len(c_syntaxes) > 0:
                c_syntax = c_syntaxes.pop(0)
                accession = BioDataHandler.retrieve_accession(c_syntax)
                o["csyntax"] = c_syntax
                if accession is not None:
                    o["geneAccession"] = accession

            if len(p_syntaxes) > 0:
                o["psyntax"] = p_syntaxes.pop(0)

            aliases = c_syntaxes + p_syntaxes
            if len(aliases) > 0:
                o["aliases"] = aliases

            logging.debug("line %d : %s", line, json.dumps(o, default=json_util.default))
            self.repository.persist(o)
        except ParseError as e:
            logging.error("Error processing %d (%s,%s)", line, e.code,
                          e.position)
