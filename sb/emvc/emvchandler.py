__author__ = "Biju Joseph"
import json
import logging

from bson import json_util
from dateutil import parser

from sb.bio.parserutils import BioDataHandler

logger = logging.getLogger('emvc')


# Handles EMVClass data with minimal transformations
class EMVClassDataHandler(BioDataHandler):
    """Provides functionality to process EmVClass records, transform to JSON and persist it
    in MongoDB database ingest.
    Attributes:
        datafile: a CSV file, EMVClass published by Emory Genetics Laboratory
        repository: a repository to use for persisting json records
    """

    def __init__(self, datafile, repo):
        """ Initializes the handler with basic CSV to JSON key mapping """
        super(EMVClassDataHandler, self).__init__(datafile)
        self.repository = repo
        self.HEADER_MAPPING = {
            "Order": "order",
            "approved_symbol": "gene",
            "Exon": "exon",
            "EGL_VARIANT": "csyntax",
            "EGL_PROTEIN": "psyntax",
            "EGL_CLASSIFICATION": "classification",
            "EGL_CLASSIFICATION_DATE": "reviewedOn",
            "VARIANT_AKA_LIST": "aliases",
            "CLINVAR_RCV": "clinVarRCV"
        }

    @staticmethod
    def transfrom(key, value):
        """ perform data type conversion on sytax aliases and review date
        :param key: the CSV field name
        :param value: the CSV data element
        :return: value to be stored in json
        """
        if key == "EGL_CLASSIFICATION_DATE":
            return parser.parse(value)
        elif key == "VARIANT_AKA_LIST":
            return value.split("|")
        else:
            return value

    def on_data(self, line, row):
        """
        Will dissect the row data and create JSON object
        :param line: the line number in CSV file
        :param row: the data line in CSV
        """
        o = {}
        logging.debug(row)
        for field_name in self.HEADER_MAPPING.keys():
            field_value = row[field_name]
            if field_value is None or field_value == "":
                continue

            key = self.HEADER_MAPPING[field_name]
            value = EMVClassDataHandler.transfrom(field_name, field_value)
            o[key] = value

            # find gene accession from csyntax
            if key == "csyntax":
                accession = BioDataHandler.retrieve_accession(value)
                if accession is not None:
                    o["geneAccession"] = accession

        logging.debug("line %d : %s", line, json.dumps(o, default=json_util.default))
        self.repository.persist(o)
        del o
