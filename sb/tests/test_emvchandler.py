#!/usr/bin/python
__author__ = "Biju Joseph"
from unittest import TestCase

import pymongo

from sb.bio.parserutils import BioCSVParser
from sb.emvc.emvchandler import EMVClassDataHandler
from sb.repository.mongorepository import MongoDbRepository


class TestEMVClassDataHandler(TestCase):
    """ Test the parsing and data handling functionality """

    def setUp(self):
        self.client = pymongo.MongoClient()
        self.db = self.client.bio
        self.db["tests"].delete_many({})
        self.repo = MongoDbRepository(self.db, "tests")

    # Example row:
    # Order	approved_symbol	VARIANT_ID	Exon	EGL_VARIANT	                EGL_PROTEIN	 EGL_CLASSIFICATION	EGL_CLASSIFICATION_DATE	 VARIANT_AKA_LIST	CLINVAR_RCV
    # 2	    AARS	        24909	    Ex19	NM_001605.2:c.2521-3C>T		VOUS	     11/23/2014	        NM_001605.2:c.2521-3C>T | NM_058219.2:c.-1903C>T | XM_005255813.1:c.2521-3C>T
    def test_on_data(self):
        self.handler = EMVClassDataHandler('test.csv', self.repo)
        self.parser = BioCSVParser(self.handler)
        self.parser.parse()

        docs = self.repo.load({"$or": [{"exon": "Ex19"}, {"exon": "Ex6"}]},
                              [("reviewedOn", pymongo.ASCENDING)])
        self.assertEqual(2, len(docs))

        docs = self.repo.load({"exon": "Ex19"},
                              [("reviewedOn", pymongo.ASCENDING)])
        self.assertEqual(1, len(docs))
        doc = docs[0]
        self.assertEqual("NM_001605.2", doc["geneAccession"])
        self.assertEqual("VOUS", doc["classification"])
        self.assertEqual("Ex19", doc["exon"])
        self.assertEqual("AARS", doc["gene"])
        self.assertEqual("NM_001605.2:c.2521-3C>T", doc["csyntax"])
