import logging
from unittest import TestCase

from sb.repository.repositories import BatchRepository
from sb.repository.repositories import Repository

logging.basicConfig(level='DEBUG',
                    format='%(levelname).1s %(created)f %(filename)s:%(lineno)s [%(funcName)s] %(message)s')


class TestBatchRepository(TestCase):
    def test_persist(self):
        test_repo = MyTestRepository("test")
        b = BatchRepository(3, test_repo)
        b.persist({"x": 1})
        b.persist({"x": 2})
        b.persist({"x": 3})
        b.persist({"x": 4})
        b.persist({"x": 5})
        b.persist({"x": 6})
        b.persist({"x": 7})  # Note, the caller must invoke flush before exit
        self.assertEqual(2, test_repo.get_persists())

        # This is the ideal usage of BatchRepository, where the __exit__ function takes care flushing
        test_repo = MyTestRepository("test")
        with BatchRepository(3, test_repo) as repo:
            repo.persist({"a": 1})
            repo.persist({"a": 2})
            repo.persist({"a": 3})
            repo.persist({"a": 4})
            repo.persist({"a": 5})
        self.assertEqual(2, test_repo.get_persists())

        # This test shows that flush happens even if batch is not full
        test_repo = MyTestRepository("test")
        with BatchRepository(3, test_repo) as repo:
            repo.persist({"a": 1})
        self.assertEqual(1, test_repo.get_persists())

    def test_load(self):
        test_repo = MyTestRepository("test")
        b = BatchRepository(3, test_repo)
        self.assertEqual(0, test_repo.get_loads())
        b.load({})
        b.load({})
        self.assertEqual(2, test_repo.get_loads())


class MyTestRepository(Repository):
    def __init__(self, name):
        super().__init__(name)
        self.persist_count = 0
        self.load_count = 0

    def persist(self, obj):
        self.persist_count += 1
        return self.persist_count

    def load(self, selector, sort=[]):
        self.load_count += 1
        return {"cnt": self.load_count}

    def get_loads(self):
        return self.load_count

    def get_persists(self):
        return self.persist_count
