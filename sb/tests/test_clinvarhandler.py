import logging
from unittest import TestCase

import pymongo

from sb.bio.parserutils import BioXMLSnifferParser
from sb.clinvar.clinvarhandler import ClinVarDataHandler
from sb.repository.mongorepository import MongoDbRepository


class TestClinVarDataHandler(TestCase):
    def setUp(self):
        logging.basicConfig(level='DEBUG',
                            format='%(levelname).1s %(created)f %(filename)s:%(lineno)s [%(funcName)s] %(message)s')
        self.client = pymongo.MongoClient()
        self.db = self.client.bio
        self.db["tests"].delete_many({})
        self.repo = MongoDbRepository(self.db, "tests")

    # Example data
    # {"psyntax": "NP_000401.1:p.Cys282Tyr", "start": "26092913", "classification": "risk factor",
    # "exon": "Ex4", "gene": "HFE", "stop": "26092913", "clinVarRCV": "RCV000000021",
    # "alt": "A", "geneAccession": "NM_139011.2", "csyntax": "NM_139011.2:c.77-206G>A", "ref": "G",
    # "aliases": ["NM_000410.3:c.845G>A"], "chr": "6"}
    # {"gene": "MAOA", "geneAccession": "NM_000240.3", "start": "43683572", "ref": "C", "chr": "X",
    # "clinVarRCV": "RCV000190424", "psyntax": "NP_000231.1:p.Arg45Trp", "classification": "Pathogenic",
    # "csyntax": "NM_000240.3:c.133C>T",  "stop": "43683572", "alt": "T"}
    def test_on_data(self):
        self.handler = ClinVarDataHandler('clinvar.zip', self.repo)
        self.parser = BioXMLSnifferParser("ClinVarSet", self.handler)
        self.parser.parse()

        docs = self.repo.load({}, [("26092913", pymongo.ASCENDING)])
        self.assertEqual(2, len(docs))

        doc1 = docs[0]
        self.assertEqual("HFE", doc1["gene"])
        self.assertEqual("26092913", doc1["start"])
        self.assertEqual("26092913", doc1["stop"])
        self.assertEqual("A", doc1["alt"])
        self.assertEqual("G", doc1["ref"])
        self.assertEqual("6", doc1["chr"])
        self.assertEqual("NM_139011.2", doc1["geneAccession"])
        self.assertEqual("RCV000000021", doc1["clinVarRCV"])
        self.assertEqual("Ex4", doc1["exon"])
        self.assertEqual("risk factor", doc1["classification"])
        self.assertEqual("NP_000401.1:p.Cys282Tyr", doc1["psyntax"])

        doc2 = docs[1]
        self.assertEqual("MAOA", doc2["gene"])
        self.assertEqual("43683572", doc2["start"])
        self.assertEqual("43683572", doc2["stop"])
        self.assertEqual("T", doc2["alt"])
        self.assertEqual("C", doc2["ref"])
        self.assertEqual("X", doc2["chr"])
        self.assertEqual("NM_000240.3", doc2["geneAccession"])
        self.assertEqual("RCV000190424", doc2["clinVarRCV"])
        self.assertEqual("Pathogenic", doc2["classification"])
        self.assertEqual("NP_000231.1:p.Arg45Trp", doc2["psyntax"])
