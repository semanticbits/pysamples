#!/usr/bin/python
__author__ = "Biju Joseph"

from unittest import TestCase

from sb.bio.parserutils import BioDataHandler
from sb.bio.parserutils import BioXMLSnifferParser


class TestBioXMLSnifferParser(TestCase):
    def test_parse(self):
        h = MyTestHandler("test1.xml")
        sniffer = BioXMLSnifferParser("node", h)
        sniffer.parse()
        self.assertEquals(2, len(h.rows))
        self.assertTrue(h.begin)
        self.assertTrue(h.end)
        self.assertTrue("<node id=\"1\">" in h.rows[0])
        self.assertFalse("<node id=\"1\">" in h.rows[1])
        self.assertTrue("<node id=\"2\">" in h.rows[1])
        self.assertFalse("<node id=\"2\">" in h.rows[0])

        h = MyTestHandler("test1.xml")
        sniffer = BioXMLSnifferParser("el", h)
        sniffer.parse()
        self.assertEquals(3, len(h.rows))
        self.assertTrue(h.begin)
        self.assertTrue(h.end)


class MyTestHandler(BioDataHandler):
    def __init__(self, xmlfile):
        self.xmlfile = xmlfile

    def on_end(self, aparser):
        self.end = True

    def on_data(self, line, data):
        self.rows.append(data.strip());

    def on_begin(self, aparser):
        self.rows = []
        self.begin = True
