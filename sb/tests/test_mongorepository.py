#!/usr/bin/python
__author__ = "Biju Joseph"
from unittest import TestCase

import pymongo

from sb.repository.mongorepository import MongoDbRepository


class TestMongoDbRepository(TestCase):
    """
     Will test the MongDB repository
    """

    def setUp(self):
        self.client = pymongo.MongoClient()
        self.db = self.client.bio
        self.db["tests"].delete_many({})
        self.repo = MongoDbRepository(self.db, "tests")

    def test_persist(self):
        self.repo.persist({"_id": 55, "name": "Sam", "age": 10, "grp": 22})
        self.repo.persist({"_id": 56, "name": "Ben", "age": 12, "grp": 22})
        self.repo.persist({"_id": 57, "name": "Zak", "age": 13, "grp": 22})
        self.assertEqual(3, self.db["tests"].count({}))
        self.assertEquals(1, self.db["tests"].count({"_id": 57}))
        self.assertEquals(1, self.db["tests"].count({"name": "Ben"}))
        self.assertEquals(1, self.db["tests"].count({"name": "Ben", "grp": 22}))

    def test_load(self):
        self.db["tests"].insert_one({"x": "y", "a": 1})
        self.db["tests"].insert_one({"x": "z", "a": 2})
        self.assertEqual("y", self.repo.load({"a": 1},
                                             [("x", pymongo.ASCENDING)])[0][
            "x"])
        self.assertEqual(2, self.repo.load({"a": 2})[0]["a"])
        docs = self.repo.load({"$or": [{"x": "y"}, {"x": "z"}]}, [("x", pymongo.ASCENDING)])
        self.assertEqual(2, len(docs))
