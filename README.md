# Bio Utilities

# Prerequisites
 You need mongodb (locally running)
 You must install python libraries for mongo-client 
 
 ```
   python -m pip install pymongo
 ```
 
 and date-util
 
 ```
   python -m pip install python-dateutil
```
# Test files
 All the tests are available in *$workspace\pysamples\sb\tests* folder, _emvchandler_test.py_ is the one that parses and load data into a MongoDB Collection
 
# Package Structure
 1. bio - This package has "parserutils" module which defines the base parser/handler classes
 2. emvc - This package has "emvchandler" module which handles EMVClass
 3. repository - This package have "repositories" and "mongorepository" modules
 4. tests - This package have all the test files. 
